class CreateOrderItemDto {
  productId: number;
  amount: number;
}

import { IsNotEmpty } from 'class-validator';
export class CreateOrderDto {
  @IsNotEmpty()
  customerId: number;

  @IsNotEmpty()
  orderItems: CreateOrderItemDto[];
}
